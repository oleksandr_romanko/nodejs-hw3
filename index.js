const express = require('express');
const morgan = require('morgan');
const config = require('config');
const mongoose = require('mongoose');
const authRoutes = require('./routes/auth.routes');
const userRoutes = require('./routes/user.routes');
const truckRoutes = require('./routes/truck.routes');
const loadRoutes = require('./routes/load.routes');
const path = require('path');
const rfs = require('rotating-file-stream');
const app = express();

app.use(express.static(path.resolve(__dirname, 'client')));

const accessLogStream = rfs.createStream('access.log', {
  interval: '1d',
  path: path.join(__dirname, 'log'),
});

app.use(express.json());
app.use(express.urlencoded({extended: false}));
app.use(morgan('common', {stream: accessLogStream}));
app.use('/api/auth', authRoutes);
app.use('/api/users', userRoutes);
app.use('/api/trucks', truckRoutes);
app.use('/api/loads', loadRoutes);

const PORT = config.get('port') || 8080;
const start = async () => {
  try {
    await mongoose.connect(config.get('mongoUri'), {
      useNewUrlParser: true,
      useUnifiedTopology: true,
      useFindAndModify: false,
      useCreateIndex: true,
    });

    app.listen(PORT, () => {
      console.log(`Server has been started on port ${PORT}...`);
    });
  } catch (e) {
    console.log('Server Error', e.message);
    process.exit(1);
  }
};

start();
