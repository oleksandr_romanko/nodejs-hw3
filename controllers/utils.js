module.exports.truckParameteres = (payload, dimensions) => {
  const {width, length, height} = dimensions;
  const types = [
    {
      type: 'SPRINTER',
      payload: 1700,
      width: 300,
      length: 250,
      height: 170,
    },
    {
      type: 'SMALL STRAIGHT',
      payload: 2500,
      width: 500,
      length: 250,
      height: 170,
    },
    {
      type: 'LARGE STRAIGHT',
      payload: 4000,
      width: 700,
      length: 350,
      height: 200,
    },
  ];
  return types.find((i) =>
    i.payload >= payload &&
    i.width >= width &&
    i.length >= length &&
    i.height >= height);
};

module.exports.loadState = (state) => {
  const states = [
    'En route to Pick Up',
    'Arrived to Pick Up',
    'En route to delivery',
    'Arrived to delivery'];
  let result = {
    nextState: states[0],
    idx: 0,
  };
  if (state) {
    result = {
      nextState: states[states.indexOf(state) + 1],
      idx: states.indexOf(state) + 1};
  }
  return result;
};
