const Load = require('../models/Load');
const Truck = require('../models/Truck');
const {truckParameteres, loadState} = require('./utils');

module.exports.getLoads = async (req, res) => {
  try {
    const {_id, role} = req.user;
    const query = role === 'DRIVER' ?
                      {assigned_to: _id, status: {$in: ['POSTED', 'SHIPPED']}} :
                      {created_by: _id};
    const loads = await Load.find(query);
    if (!loads.length) {
      return res.status(400).json({message: 'Loads has no found'});
    }
    res.json({loads});
  } catch (e) {
    return res.status(500).json({message: 'Server error'});
  }
};

module.exports.addLoad = async (req, res) => {
  try {
    const {_id, role} = req.user;
    const body = {created_by: _id, ...req.body};
    if (role !== 'SHIPPER') {
      return res.status(400)
          .json({message: 'Only SHIPPER is able to add Loads'});
    }
    const load = new Load(body);
    await load.save();
    res.json({message: 'Load created successfully'});
  } catch (e) {
    return res.status(500).json({message: 'Server error'});
  }
};

module.exports.getActiveLoad = async (req, res) => {
  try {
    const {_id, role} = req.user;
    if (role !== 'DRIVER') {
      return res.status(400)
          .json({message: 'Only DRIVER is able to get active Loads'});
    }
    const load = await Load.findOne({
      assigned_to: _id,
      status: {$eq: 'ASSIGNED'},
    });
    if (!load) {
      return res.status(400).json({message: 'Load has no found'});
    }
    res.json({load});
  } catch (e) {
    return res.status(500).json({message: 'Server error'});
  }
};

module.exports.iterateLoadState = async (req, res) =>{
  try {
    const {_id, role} = req.user;
    if (role !== 'DRIVER') {
      return res.status(400)
          .json({message: 'Only DRIVER is able to iterate load state'});
    }
    const truck = await Truck.findOne({
      assigned_to: _id,
      status: {$eq: 'OL'},
    });
    if (!truck) {
      return res.status(400).json({message: 'DRIVER has no any ASSIGNED load'});
    }
    const {state} = await Load.findOne({
      assigned_to: _id,
      status: {$eq: 'ASSIGNED'},
    });
    const {nextState, idx} = loadState(state);
    const loadUpdate = {state: nextState};
    if (idx > 2 ) {
      await Truck.findOneAndUpdate(
          {assigned_to: _id, status: {$eq: 'OL'}},
          {status: 'IS'},
      );
      loadUpdate.status = 'SHIPPED';
    };

    const load = await Load.findOneAndUpdate(
        {assigned_to: _id, status: {$eq: 'ASSIGNED'}},
        loadUpdate,
        {new: true},
    );
    res.json({message: `Load state changed to ${load.state}`});
  } catch (e) {
    console.log(e);
    return res.status(500).json({message: 'Server error'});
  }
};

module.exports.getLoadById = async (req, res) =>{
  try {
    const {_id, role} = req.user;
    const {id} = req.params;
    const query = role === 'DRIVER' ? {_id: id, assigned_to: _id} :
                                      {_id: id, created_by: _id};
    const load = await Load.findOne(query);
    if (!load) {
      return res.status(400).json({message: 'Load has no found'});
    }
    res.json({load});
  } catch (e) {
    return res.status(500).json({message: 'Server error'});
  }
};

module.exports.updateLoadById = async (req, res) =>{
  try {
    const {role} = req.user;
    const {id} = req.params;
    const body = req.body;
    if (role !== 'SHIPPER') {
      return res.status(400)
          .json({message: 'Only SHIPPER is able to update load'});
    }
    const load = await Load.findByIdAndUpdate({_id: id, created_by: _id}, body);
    if (!load) {
      return res.status(400).json({message: 'Load has no found'});
    }
    res.json({message: 'Load details changed successfully'});
  } catch (e) {
    console.log(e);
    return res.status(500).json({message: 'Server error'});
  }
};

module.exports.deleteLoadById = async (req, res) =>{
  try {
    const {_id, role} = req.user;
    const {id} = req.params;
    if (role !== 'SHIPPER') {
      return res.status(400)
          .json({message: 'Only SHIPPER is able to delete Load'});
    }
    const load = await Load.findOneAndDelete({_id: id, created_by: _id});
    if (!load) {
      return res.status(400).json({message: 'Load has no found'});
    }
    res.json({message: 'Load deleted successfully'});
  } catch (e) {
    return res.status(500).json({message: 'Server error'});
  }
};

module.exports.postLoadById = async (req, res) =>{
  try {
    const {_id, role} = req.user;
    const {id} = req.params;
    if (role !== 'SHIPPER') {
      return res.status(400)
          .json({message: 'Only SHIPPER is able to post Load'});
    }
    const load = await Load.findOneAndUpdate(
        {_id: id, created_by: _id},
        {status: 'POSTED'},
        {new: true},
    );
    if (!load) {
      return res.status(400).json({message: 'Load has no found'});
    }
    const {type} = truckParameteres(load.payload, load.dimensions);
    const truck = await Truck.findOneAndUpdate(
        {assigned_to: {$exists: true}, status: {$eq: 'IS'}, type: {$eq: type}},
        {status: 'OL'},
        {new: true},
    );
    if (!truck) {
      return res.status(400)
          .json({message: 'Truck not found to match the parameters'});
    }
    await Load.findOneAndUpdate(
        {_id: id, created_by: _id},
        {
          assigned_to: truck.assigned_to,
          status: 'ASSIGNED',
          state: 'En route to Pick Up',
        },
        {new: true},
    );
    res.json({message: 'Load posted successfully', driver_found: true});
  } catch (e) {
    console.log(e);
    return res.status(500).json({message: 'Server error'});
  }
};

module.exports.getShippingInfoById = async (req, res) =>{
  try {
    const {_id, role} = req.user;
    const {id} = req.params;
    if (role !== 'SHIPPER') {
      return res.status(400)
          .json({message: 'Only SHIPPER is able to post Load'});
    }
    const load = await Load.findOne({
      _id: id,
      created_by: _id,
      status: {$eq: 'SHIPPED'},
    });
    if (!load) {
      return res.status(400).json({message: 'SHIPPED has no found'});
    }
    res.json({load});
  } catch (e) {
    return res.status(500).json({message: 'Server error'});
  }
};
