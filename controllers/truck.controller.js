const Truck = require('../models/Truck');

module.exports.getTrucks = async (req, res) => {
  try {
    const {_id} = req.user;
    const trucks = await Truck.find({created_by: _id});
    if (!trucks.length) {
      return res.status(400).json({message: 'Trucks has no found'});
    }
    res.json({trucks});
  } catch (e) {
    return res.status(500).json({message: 'Server error'});
  }
};

module.exports.addTruck = async (req, res) => {
  try {
    const {type} = req.body;
    const {_id, role} = req.user;
    if (role !== 'DRIVER') {
      return res.status(400)
          .json({message: 'Only DRIVER is able to add trucks'});
    }
    const assined = await Truck.findOne({assigned_to: _id});
    const query = !assined ?
      {created_by: _id, assigned_to: _id, type}:
      {created_by: _id, type};

    const truck = new Truck(query);
    await truck.save();
    res.json({message: 'Truck created successfully'});
  } catch (e) {
    console.log(e);
    return res.status(500).json({message: 'Server error'});
  }
};

module.exports.getTruckById = async (req, res) => {
  try {
    const {_id, role} = req.user;
    const {id} = req.params;
    if (role !== 'DRIVER') {
      return res.status(400)
          .json({message: 'Only DRIVER is able to add trucks'});
    }
    const truck = await Truck.find({_id: id, created_by: _id});
    if (!truck) {
      return res.status(400).json({message: 'Truck has no found'});
    }
    res.json({truck});
  } catch (e) {
    return res.status(500).json({message: 'Server error'});
  }
};

module.exports.updateTruckById = async (req, res) =>{
  try {
    const {type} = req.body;
    const {id} = req.params;
    const {_id, role} = req.user;
    if (role !== 'DRIVER') {
      return res.status(400)
          .json({message: 'Only DRIVER is able to update trucks'});
    }
    const truck = await Truck
        .findOneAndUpdate({_id: id, created_by: _id}, {type});
    if (!truck) {
      return res.status(400).json({message: 'Truck has no found'});
    }
    res.json({message: 'Truck details changed successfully'});
  } catch (e) {
    return res.status(500).json({message: 'Server error'});
  }
};

module.exports.deleteTruckById = async (req, res) =>{
  try {
    const {_id, role} = req.user;
    const {id} = req.params;
    if (role !== 'DRIVER') {
      return res.status(400)
          .json({message: 'Only DRIVER is able to delete trucks'});
    }
    const truck = await Truck.findOneAndDelete({_id: id, created_by: _id});
    if (!truck) {
      return res.status(400).json({message: 'Truck has no found'});
    }
    res.json({message: 'Truck deleted successfully'});
  } catch (e) {
    return res.status(500).json({message: 'Server error'});
  }
};

module.exports.assignTruckById = async (req, res) =>{
  try {
    const {_id, role} = req.user;
    const {id} = req.params;
    if (role !== 'DRIVER') {
      return res.status(400)
          .json({message: 'Only DRIVER is able to assign trucks'});
    }
    const assined = await Truck.findOne({assined_to: id});
    if (assined) {
      return res.status(400)
          .json({message: 'The user is already assigned to one truck'});
    }
    const truck = await Truck
        .findOneAndUpdate(
            {assined_to: {$exists: false}, created_by: _id},
            {assined_to: id},
            {new: true})
        .exec();
    if (!truck) {
      return res.status(400).json({message: 'Truck has no found'});
    }
    res.json({message: 'Truck assigned successfully'});
  } catch (e) {
    console.log(e);
    return res.status(500).json({message: 'Server error'});
  }
};
