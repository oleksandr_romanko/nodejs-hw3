const bcrypt = require('bcryptjs');
const User = require('../models/User');

module.exports.getProfile = async (req, res, next) => {
  const {_id, email, createdDate} = {...req.user, createdDate: created_date};
  res.json({user: {_id, email, created_date: createdDate}});
  next();
};

module.exports.deleteProfile = async (req, res, next) => {
  try {
    const {_id} = req.user;
    await User.findByIdAndRemove({_id});
    res.json({message: 'Profile deleted successfully'});
  } catch (e) {
    return res.status(500).json({message: 'Server error'});
  }
};

module.exports.changePassword = async (req, res) => {
  try {
    const {oldPassword, newPassword} = req.body;
    const {password, _id} = req.user;
    const isMatched = await bcrypt.compare(oldPassword, password);
    if (!isMatched) {
      return res.status(400).json({message: 'The old password has no match'});
    }
    const hashedPassword = await bcrypt.hash(newPassword, 12);
    await User.findByIdAndUpdate({_id}, {password: hashedPassword});
    res.json({message: 'Password changed successfully'});
  } catch (e) {
    console.log(e);
    return res.status(500).json({message: 'Server error'});
  }
};
