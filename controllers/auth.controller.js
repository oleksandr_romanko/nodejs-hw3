const bcrypt = require('bcryptjs');
const jwt = require('jsonwebtoken');
const config = require('config');
const User = require('../models/User');

module.exports.register = async (req, res) => {
  try {
    const {email, password, role} = req.body;
    const candidate = await User.findOne({email});
    if (candidate) {
      return res.status(400).json({message: 'User has already exists'});
    }
    const hashedPassword = await bcrypt.hash(password, 12);
    const user = new User({email, password: hashedPassword, role});
    await user.save();
    res.json({message: 'User has been registered succesfully'});
  } catch (e) {
    console.log(e);
    return res.status(500).json({message: 'Server error'});
  }
};

module.exports.login = async (req, res) => {
  try {
    const {email, password} = req.body;
    const user = await User.findOne({email});
    if (!user) {
      return res.status(400).json({message: 'User has no found'});
    }
    const isMatched = await bcrypt.compare(password, user.password);
    if (!isMatched) {
      return res.status(400).json({message: 'Wrong password'});
    }
    const token = jwt.sign({_id: user._id}, config.get('jwt_secret'));
    res.json({message: 'Succses', jwt_token: token});
  } catch (e) {
    console.log(e);
    return res.status(500).json({message: 'Server error'});
  }
};
