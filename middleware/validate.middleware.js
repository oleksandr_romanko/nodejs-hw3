const Joi = require('joi');

module.exports.registerValidate = async (req, res, next) => {
  const schema = Joi.object({
    email: Joi.string()
        .email({minDomainSegments: 2}),

    password: Joi.string()
        .pattern(new RegExp('^[a-zA-Z0-9]{6,30}$'))
        .required(),

    role: Joi.string()
        .required()
        .pattern(new RegExp('^SHIPPER|DRIVER$')),

  });

  try {
    await schema.validateAsync(req.body);
    next();
  } catch (err) {
    return res.status(400).json({message: err.message});
  }
};

module.exports.loginValidate = async (req, res, next) => {
  const schema = Joi.object({
    email: Joi.string()
        .email({minDomainSegments: 2}),

    password: Joi.string()
        .pattern(new RegExp('^[a-zA-Z0-9]{6,30}$'))
        .required(),
  });

  try {
    await schema.validateAsync(req.body);
    next();
  } catch (err) {
    return res.status(400).json({message: err.message});
  }
};


module.exports.userPatchValidate = async (req, res, next) => {
  const schema = Joi.object({
    oldPassword: Joi.string()
        .pattern(new RegExp('^[a-zA-Z0-9]{6,30}$')),

    newPassword: Joi.string()
        .pattern(new RegExp('^[a-zA-Z0-9]{6,30}$')),
  });

  try {
    await schema.validateAsync(req.body);
    next();
  } catch (err) {
    return res.status(400).json({message: err.message});
  }
};

module.exports.addTruckValidate = async (req, res, next) => {
  const schema = Joi.object({
    type: Joi.string()
        .required()
        .pattern(new RegExp('^SPRINTER|SMALL STRAIGHT|LARGE STRAIGHT$')),
  });

  try {
    await schema.validateAsync(req.body);
    next();
  } catch (err) {
    return res.status(400).json({message: err.message});
  }
};
