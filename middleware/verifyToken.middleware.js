const jwt = require('jsonwebtoken');
const config = require('config');
const User = require('../models/User');

module.exports = async (req, res, next) => {
  try {
    const token = req.headers.authorization ?
                  req.headers.authorization.split(' ')[1] : false;
    if (!token) {
      return res.status(400).json({message: 'User is not authorized'});
    }
    const {_id} = jwt.verify(token, config.get('jwt_secret'));
    const user = await User.findById({_id});
    if (!user) {
      return res.status(400).json({message: 'User has no found'});
    }
    req.user = user;
    next();
  } catch (e) {
    console.log(e);
    return res.status(500).json({message: 'Server error'});
  }
};
