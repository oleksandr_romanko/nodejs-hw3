const {Schema, model} = require('mongoose');

const truckSchema = new Schema({
  created_by: {
    type: Schema.Types.ObjectId,
    ref: 'User',
    required: true,
  },
  assigned_to: {
    type: Schema.Types.ObjectId,
    ref: 'User',
  },
  type: {
    type: String,
    enum: ['SPRINTER', 'SMALL STRAIGHT', 'LARGE STRAIGHT'],
    required: true,
  },
  status: {
    type: String,
    enum: ['OL', 'IS'],
    required: true,
    default: 'IS',
  },
  created_date: {
    type: Date,
    default: Date.now,
  },
});

module.exports = model('Truck', truckSchema);
