const express = require('express');
const {
  registerValidate,
  loginValidate} = require('../middleware/validate.middleware');
const {register, login} = require('../controllers/auth.controller');
const router = new express.Router();

// api/auth
router.post('/register', registerValidate, register);

router.post('/login', loginValidate, login);

module.exports = router;
