const express = require('express');
const verifyToken = require('../middleware/verifyToken.middleware');
// const {addTruckValidate} = require('../middleware/validate.middleware');
const {
  getLoads,
  addLoad,
  getActiveLoad,
  iterateLoadState,
  getLoadById,
  updateLoadById,
  deleteLoadById,
  postLoadById,
  getShippingInfoById} = require('../controllers/load.controller');
const router = new express.Router();

// api/loads
router.get('/', verifyToken, getLoads);

router.post('/', verifyToken, addLoad);

router.get('/active', verifyToken, getActiveLoad);

router.patch('/active/state', verifyToken, iterateLoadState);

router.get('/:id', verifyToken, getLoadById);

router.put('/:id', verifyToken, updateLoadById);

router.put('/:id', verifyToken, deleteLoadById);

router.post('/:id/post', verifyToken, postLoadById);

router.get('/:id/shipping_info', verifyToken, getShippingInfoById);

module.exports = router;
