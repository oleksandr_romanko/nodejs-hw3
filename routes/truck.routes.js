const express = require('express');
const verifyToken = require('../middleware/verifyToken.middleware');
const {addTruckValidate} = require('../middleware/validate.middleware');
const {
  getTrucks,
  addTruck,
  getTruckById,
  updateTruckById,
  deleteTruckById,
  assignTruckById} = require('../controllers/truck.controller');
const router = new express.Router();

// api/truck
router.get('/', verifyToken, getTrucks);

router.post('/', addTruckValidate, verifyToken, addTruck);

router.get('/:id', verifyToken, getTruckById);

router.put('/:id', verifyToken, updateTruckById);

router.delete('/:id', verifyToken, deleteTruckById);

router.post('/:id/assign', verifyToken, assignTruckById);

module.exports = router;
