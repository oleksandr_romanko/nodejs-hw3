const express = require('express');
const {userPatchValidate} = require('../middleware/validate.middleware');
const verifyToken = require('../middleware/verifyToken.middleware');
const {
  getProfile,
  deleteProfile,
  changePassword} = require('../controllers/user.controller');
const router = new express.Router();

// /api/users/me
router.get('/me', verifyToken, getProfile);

router.delete('/me', verifyToken, deleteProfile);

router.patch('/me/password', userPatchValidate, verifyToken, changePassword);

module.exports = router;
